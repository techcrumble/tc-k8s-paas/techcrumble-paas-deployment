#!/bin/bash

ENVIRONMENT_VERSION=$(aws ssm get-parameter --region=us-east-1 --name "/PAAS/REPO/ENVIRONMENT_VERSION" | jq -r '.Parameter.Value')
ENVIRONMENT_REPO=./TechCrumble-PaaS-Environments

ENVIRONMENT_REPO_URL=https://github.com/ArunaLakmal/TechCrumble-PaaS-Environments.git

git clone "$ENVIRONMENT_REPO_URL" --branch "$ENVIRONMENT_VERSION" --single-branch
cd "$ENVIRONMENT_REPO"
make certs